return {
  { "nvim-lualine/lualine.nvim", name = "lualine", opts = { theme = "catppuccin" } },
  { "nvim-lualine/lualine.nvim", dependencies = { "nvim-tree/nvim-web-devicons" } },
}
